#define _WINSOCK_DEPRECATED_NO_WARNINGS

#include "Server.h"
#include <exception>
#include <iostream>
#include <string>

std::mutex fileMtx;
std::mutex usersMtx;
std::mutex msgMtx;
std::mutex msgHandlerMtx;
std::condition_variable newMsgAdded;

Server::Server()
{
	_serverSocket = socket(AF_INET,  SOCK_STREAM,  IPPROTO_TCP); 

	if (_serverSocket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__ " - socket");
}

Server::~Server()
{
	try
	{
		closesocket(_serverSocket);
	}
	catch (...) {}
}

void Server::serve(int& port, string& ip)
{
	struct sockaddr_in sa = { 0 };
	
	sa.sin_port = htons(port); // Port that the server will listen for.
	sa.sin_family = AF_INET;   
	sa.sin_addr.s_addr = inet_addr(ip.c_str()); // Server ip.

	// Stepping out to the global namespace.
	// Connects between the socket and the configuration (port and etc..).
	if (bind(_serverSocket, (struct sockaddr*)&sa, sizeof(sa)) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - bind");
	
	// Start listening for incoming requests of clients.
	if (listen(_serverSocket, SOMAXCONN) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - listen");
	cout << "Listening..." << endl;

	// Create a new thread to handle messages.
	std::thread messagesT(&Server::messageHandler, this);

	while (true)
	{
		// The main thread accepts new clients..
		cout << "Waiting for client connection request..." << endl;

		// Accept connection with new client and create new client thread.
		accept();
	}
}


void Server::accept()
{
	std::unique_lock<std::mutex> usersLock(usersMtx, std::defer_lock);
	int nameLen = 0;
	string name = "";

	// Accept the client and create a specific socket from server to this client.
	SOCKET client_socket = ::accept(_serverSocket, NULL, NULL);

	if (client_socket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__);

	// Get the login information from the client socket.
	Helper::getMessageTypeCode(client_socket);
	nameLen = Helper::getIntPartFromSocket(client_socket, USERNAME_LEN);
	name = Helper::getStringPartFromSocket(client_socket, nameLen);

	usersLock.lock();

	// If the name already exists, don't allow the client to connect.
	if (connectedUsers.find(name) != connectedUsers.end())
	{
		usersLock.unlock();
		closesocket(client_socket); // Close the client's socket.
		cerr << "Client with name " << name << " already exists." << endl;
	}
	else // If the client is a new client:
	{
		usersLock.unlock();
		cout << "Accepted new connection with client: " << name << "." << endl;

		// Add the client's name to the set of connected users.
		usersLock.lock();
		this->connectedUsers.insert(name);
		usersLock.unlock();

		// Create a new client thread of the function that handles the conversation with the client.
		std::thread clientThread(&Server::clientHandler, this, client_socket, name);
		clientThread.detach();
	}
}


void Server::clientHandler(SOCKET clientSocket, string name)
{
	int msgCode = 0, nameLen = 0, lenSecondUser = 0, lenNewMessage = 0;
	string secondUser = "", msgData = "", chatFileName = "";
	std::ifstream chatFile;
	std::stringstream chatContent;

	try
	{
		// Send first server update message to client after login.
		Helper::send_update_message_to_client(clientSocket, "", "", Helper::getConnUsersString(connectedUsers));

		while (clientSocket != INVALID_SOCKET)
		{
			// Get client's update message.
			msgCode = Helper::getMessageTypeCode(clientSocket);

			if (msgCode != 0)
			{
				lenSecondUser = Helper::getIntPartFromSocket(clientSocket, USERNAME_LEN);
				secondUser = Helper::getStringPartFromSocket(clientSocket, lenSecondUser);
				lenNewMessage = Helper::getIntPartFromSocket(clientSocket, LEN_NEW_MESSAGE);
				msgData = Helper::getStringPartFromSocket(clientSocket, lenNewMessage);

				if (lenSecondUser != 0)
				{
					// Find the requested chat file name and create a new message object.
					chatFileName = Helper::getChatFileName(name, secondUser);

					if (lenNewMessage != 0)
					{
						Message newMessage(name, msgData, chatFileName);
						addNewMessage(newMessage);

						// Notify the message handler that a new message has been added.
						newMsgAdded.notify_one();
					}

					// Find the client's chat file with the second user, open it and get its content.
					fileMtx.lock();
					chatFile.open(chatFileName);

					if (chatFile.is_open())
					{
						chatContent << chatFile.rdbuf();
						chatFile.close();
						fileMtx.unlock();
					}
					else // If file couldn't be opened, chat content will be empty.
					{
						chatContent.str() = "";
						fileMtx.unlock();
					}

					// Send update message to client.
					Helper::send_update_message_to_client(clientSocket, chatContent.str(), secondUser, Helper::getConnUsersString(connectedUsers));
					
					// Empty the stringstream.
					chatContent.str(std::string());
				}
				else // If the user didn't send a new message, only update the connected users list.
				{
					Helper::send_update_message_to_client(clientSocket, "", "", Helper::getConnUsersString(connectedUsers));
				}
			}
		}

		// Closing the socket (in the level of the TCP protocol)
		closesocket(clientSocket);
		cout << name << " exited." << endl;

		// Remove user's name from the list of connected users.
		std::unique_lock<std::mutex> usersLock(usersMtx);
		this->connectedUsers.erase(name);
	}
	catch (const std::exception& e)
	{
		closesocket(clientSocket);
		cout << name << " exited." << endl;

		// Remove user's name from the list of connected users.
		std::unique_lock<std::mutex> usersLock(usersMtx);
		this->connectedUsers.erase(name);
	}
}

/*
Function handles sending messages to clients.
Input: none.
Output: none.
*/
void Server::messageHandler()
{
	// Wait for a new message to be added, and write it to chat file.
	while (_serverSocket != INVALID_SOCKET)
	{
		std::unique_lock<std::mutex> msgLock(msgHandlerMtx);
		newMsgAdded.wait(msgLock, [&]() {return !messages.empty(); });
		fileMtx.lock();
		messages.front().sendMessage();
		fileMtx.unlock();
		messages.pop();
	}
}

/*
Function adds a new message to the queue of messages.
Input: new message reference.
Output: none.
*/
void Server::addNewMessage(Message& newMessage)
{
	// Add new message to the queue of messages.
	std::unique_lock<std::mutex> msgLock(msgMtx);
	this->messages.push(newMessage);
}
