#pragma once

#include <string>
#include <fstream>
#include <iostream>

using std::string;
using std::cerr;
using std::endl;

class Message
{
private:
	string author;
	string data;
	string fileName;

public:
	Message(string author, string data, string fileName);
	~Message() {};
	void sendMessage();
};