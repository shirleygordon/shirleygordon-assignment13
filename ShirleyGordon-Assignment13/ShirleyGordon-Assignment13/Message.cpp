#include "Message.h"

/*
Message C'tor.
Input: author, data and fileName strings.
Output: none.
*/
Message::Message(string author, string data, string fileName)
{
	this->author = author;
	this->data = data;
	this->fileName = fileName;
}

/*
Function appends a new message to a chat file.
Input: none.
Output: none.
*/
void Message::sendMessage()
{
	string fullMsg = "&MAGSH_MESSAGE&&Author&" + author + "&DATA&" + data;
	std::ofstream chatFile;

	// Open file for appending and append the new message.
	chatFile.open(fileName, std::ios_base::app);

	if (chatFile.is_open())
	{
		chatFile << fullMsg;
		chatFile.close();
	}
	else
	{
		cerr << "Unable to open chat file " << fileName << endl;
	}
}
