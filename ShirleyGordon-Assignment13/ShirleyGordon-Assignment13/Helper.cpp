#include "Helper.h"

/*
Function gets the server port and ip from the configuration file.
Input: reference to port and ip variables.
Output: none.
*/
void Helper::getPortAndIP(int& port, string& ip)
{
	int startFrom = 0;
	std::fstream configFile("config.txt");
	string fileContent = "";
	bool found = false;

	if (configFile.is_open())
	{
		// Read configuration file into stringstream, and extract the port number and ip address.
		while (!found)
		{
			std::getline(configFile, fileContent);
			startFrom = fileContent.find("server_ip=");

			// If the ip is in the current line, extract it.
			if (startFrom != string::npos)
			{
				found = true;
				startFrom += START_FROM_IP;
				ip = fileContent.substr(startFrom, string::npos); 
				cout << "ip: " << ip << endl;
			}
		} 

		found = false;

		while (!found)
		{
			std::getline(configFile, fileContent);
			startFrom = fileContent.find("port=");

			// If the port is in the current line, extract it and convert to int.
			if (startFrom != string::npos)
			{
				found = true;
				startFrom += START_FROM_PORT;
				port = std::stoi(fileContent.substr(startFrom, string::npos));
				cout << "port: " << port << endl;
			}
		}
	}
	else // If configuration file couldn't be opened, display error and exit.
	{
		cerr << "Error opening configuration file. Please make sure it's in the same directory." << endl;
		system("PAUSE");
		exit(0);
	}
}

// recieves the type code of the message from socket (3 bytes)
// and returns the code. if no message found in the socket returns 0 (which means the client disconnected)
int Helper::getMessageTypeCode(SOCKET sc)
{
	char* s = getPartFromSocket(sc, 3);
	std::string msg(s);

	if (msg == "")
		return 0;

	int res = std::atoi(s);
	delete s;
	return  res;
}


void Helper::send_update_message_to_client(SOCKET sc, const string& file_content, const string& second_username, const string &all_users)
{
	//TRACE("all users: %s\n", all_users.c_str())
	const string code = std::to_string(MT_SERVER_UPDATE);
	const string current_file_size = getPaddedNumber(file_content.size(), 5);
	const string username_size = getPaddedNumber(second_username.size(), 2);
	const string all_users_size = getPaddedNumber(all_users.size(), 5);
	const string res = code + current_file_size + file_content + username_size + second_username + all_users_size + all_users;
	sendData(sc, res);
}

// recieve data from socket according byteSize
// returns the data as int
int Helper::getIntPartFromSocket(SOCKET sc, int bytesNum)
{
	char* s = getPartFromSocket(sc, bytesNum, 0);
	return atoi(s);
}

// recieve data from socket according byteSize
// returns the data as string
string Helper::getStringPartFromSocket(SOCKET sc, int bytesNum)
{
	char* s = getPartFromSocket(sc, bytesNum, 0);
	string res(s);
	return res;
}

// return string after padding zeros if necessary
string Helper::getPaddedNumber(int num, int digits)
{
	std::ostringstream ostr;
	ostr << std::setw(digits) << std::setfill('0') << num;
	return ostr.str();
}

/*
Function creates a string of all the connected users separated by '&'.
Input: set of all the connected users.
Output: a string of all the connected users separated by '&'.
*/
string Helper::getConnUsersString(const std::set<string> connectedUsers)
{
	string usersStr = "";
	std::set<string>::iterator usersIt = connectedUsers.begin();

	// Create the string of users.
	for (usersIt = connectedUsers.begin(); usersIt != connectedUsers.end(); usersIt++)
	{
		usersStr += (*usersIt);
		usersStr += '&';
	}

	// Remove last character from string.
	usersStr = usersStr.substr(0, usersStr.size() - 1);

	return usersStr;
}

/*
Function finds the name of the chat file between two users.
Input: the names of the two users.
Output: the name of the two users' chat file.
*/
string Helper::getChatFileName(string name1, string name2)
{
	string fileName = "";

	// First name comes before second name alphabetically.
	if (name1.compare(name2) < 0)
	{
		fileName = name1 + "&" + name2 + ".txt";
	}
	else // Second name comes before first name alphabetically.
	{
		fileName = name2 + "&" + name1 + ".txt";
	}

	return fileName;
}

// recieve data from socket according byteSize
// this is private function
char* Helper::getPartFromSocket(SOCKET sc, int bytesNum)
{
	return getPartFromSocket(sc, bytesNum, 0);
}

char* Helper::getPartFromSocket(SOCKET sc, int bytesNum, int flags)
{
	if (bytesNum == 0)
	{
		return (char*)"";
	}

	char* data = new char[bytesNum + 1];
	int res = recv(sc, data, bytesNum, flags);

	if (res == INVALID_SOCKET)
	{
		std::string s = "Error while recieving from socket: ";
		s += std::to_string(sc);
		throw std::exception(s.c_str());
	}

	data[bytesNum] = 0;
	return data;
}

// send data to socket
// this is a private function
void Helper::sendData(SOCKET sc, std::string message)
{
	const char* data = message.c_str();

	if (send(sc, data, message.size(), 0) == INVALID_SOCKET)
	{
		throw std::exception("Error while sending message to client");
	}
}