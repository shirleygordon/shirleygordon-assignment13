#pragma once

#include <WinSock2.h>
#include <Windows.h>
#include "Helper.h"
#include <thread>
#include <queue>
#include <set>
#include <mutex>
#include "Message.h"

#define USERNAME_LEN 2
#define LEN_NEW_MESSAGE 5

class Server
{
public:
	Server();
	~Server();
	void serve(int& port, string& ip);

private:

	void accept();
	void clientHandler(SOCKET clientSocket, string name);
	void messageHandler();
	void addNewMessage(Message& newMessage);

	SOCKET _serverSocket;
	std::set<string> connectedUsers; // Set containing the names of all connected users.
	std::queue<Message> messages; // Messages queue.
};

